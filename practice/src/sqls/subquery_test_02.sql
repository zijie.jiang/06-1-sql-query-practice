/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT
	MIN(orderItem.orderItemCount) AS minOrderItemCount,
	MAX(orderItem.orderItemCount) AS maxOrderItemCount,
	ROUND(avg(orderItem.orderItemCount)) AS avgOrderItemCount
FROM (
	SELECT
		COUNT(orderdetails.orderNumber) AS orderItemCount
	FROM
		orders
	LEFT JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
GROUP BY
	orders.orderNumber) orderItem;
